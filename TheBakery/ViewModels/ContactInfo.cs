﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using TheBakery.Models;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;


namespace TheBakery.ViewModels
{
    public class ContactInfo : DbContext
    {

        //[System.Web.Mvc.Remote("CheckUserName", "Account")]
        [Display(Name = "Username")]
        public string UserName { get; set; }
        [Required(ErrorMessage="Please enter your First Name.")]
        [StringLength(160)]
        [Display(Name = "First Name")]
        public string FirstName { get; set; }

        [Required(ErrorMessage="Please enter your Last Name.")]
        [StringLength(160)]
        [Display(Name = "Last Name")]        public string LastName { get; set; }
        
        [Required(ErrorMessage="Please enter your email address.")]
        [RegularExpression(@"[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}")]
        public string Email { get; set; }

        [Compare("Email")]
        [Display(Name = "Confirm Email")]        public string EmailConfirm { get; set; }

        [Range(18, 50)]
        public string Age { get; set; }

        public string Comments { get; set; }
    }
}
﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(TheBakery.Startup))]
namespace TheBakery
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}

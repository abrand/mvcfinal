﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TheBakery.Models;
using TheBakery.ViewModels;


namespace TheBakery.Controllers
{
    public class RequestInformationController : Controller
    {

        //private TheBakeryDBEntities _entities = new TheBakeryDBEntities();

        // GET: RequestInformation
        // we use this to restrict people from only being able to enter contact information if 
        // they are logged in. 
        [Authorize]
        public ActionResult RequestInformation()
        {
            return View();
        }


        //[HttpPost]
       // public ActionResult FormOne(string name)
        //{
            //Do something
            //return RedirectToAction("RequestInfoSubmitted");
        //}

        
        // Called by FormOne 
        [HttpPost] 
        public ActionResult RequestInfoSubmitted(string userName, string firstName, string lastName, string email, string emailConfirm, string age, string comments)
        {
            
            


            // Set up our ViewModel
            ContactInfo myContactInfo = new ContactInfo();
            myContactInfo.UserName = this.Request["userName"];
            myContactInfo.FirstName = this.Request["firstName"];
            myContactInfo.LastName = this.Request["lastName"];
            myContactInfo.Email = this.Request["email"];
            myContactInfo.EmailConfirm = this.Request["emailConfirm"];
            //myContactInfo.Age.ToString();
            //age.ToString();
            myContactInfo.Age = this.Request["age"];
            myContactInfo.Comments = this.Request["comments"];
            InsertContact(userName, firstName, lastName, email, emailConfirm, age, comments);

            //var db = Database.SetInitializer<TheBakeryDB>(new DropCreateDatabaseIfModelChanges<TheBakeryDB>());
            //var insertCommand = "INSERT INTO ContactInformation (username, firstName, lastName, email, age, comments) VALUES(@0, @1, @2, @3, @4, @5)";
            //db.Execute(insertCommand, userName, firstName, lastName, email, age, comments);
            

            return View(myContactInfo);

        }


        private void InsertContact(string userName, string firstName, string lastName, 
            string email, string emailConfirm, string age, string comments)
        {
            
        }

        






        


    }
}
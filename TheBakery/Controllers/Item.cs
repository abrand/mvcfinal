﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TheBakery.Controllers
{
    public class Item
    {
        public string Name { get; set; }
        public string Price { get; set; }
        public string Type { get; set; }
        public int Quantity { get; set; }
        public string Description { get; set; }
        public string Size { get; set; }
    }
}

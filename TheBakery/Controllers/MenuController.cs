﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TheBakery.Models;
using TheBakery.ViewModels;

namespace TheBakery.Controllers
{
	public class MenuController : Controller
	{

		private DefaultConnectionEntities menuDB = new DefaultConnectionEntities();

		// GET: Menu
		public ActionResult Menu()
		{


			//name = "Glazed Donuts";
			//description = "Box of six glazed donuts.";
			//price = "$4.99";
			//type = "Food";
			//size = "Medium";



			//name = "Double Chocolate Muffin";
			//description = "A single double chocolate muffin.";
			//price = "$2.99";
			//type = "Food";
			//size = "Medium";

			//name = "Iced Cookie with Sprinkles";
			//description = "Delicious iced cookie covered in sprinkles!";
			//price = "$5.99";
			//type = "Food";
			//size = "Medium";

			//name = "Coffee";
			//description = "Coffee to help with any morning!";
			//price = "$3.99";
			//type = "Beverage";
			//size = "Small";

			//name = "Hot Chocolate";
			//description = "For those cold days enjoy our hot chocolate!";
			//price = "$2.99";
			//type = "Beverage";
			//size = "Small";

			//name = new MenuItemList() { Name = "Glazed Donuts" };

			var items = new List<Item>
			{
				new Item { Name = "Glazed Donuts", Description = "Box of six glazed donuts.", Price = "$4.99", Type = "Food", Size = "Medium" },
                new Item { Name = "Double Chocolate Muffin", Description = "A single double chocolate muffin.", Price = "$2.99", Type = "Food", Size = "Small"},
                new Item { Name = "Iced Cookie with Sprinkles", Description = "Delicious iced cookie covered in sprinkles!", Price = "$5.99", Type = "Food", Size = "Medium"},
                new Item { Name = "Coffee", Description = "Coffee to help with any morning!", Price = "$3.99", Type = "Beverage", Size = "Small"},
                new Item { Name = "Hot Chocolate", Description = "For those cold days enjoy our hot chocolate!", Price = "$2.99", Type = "Beverage", Size = "Small"}
			};

			var viewModel = new MenuItemListViewModel
			{
				 Items = items
                 
			};


			//MenuItemList myMenu = new MenuItemList();

			
			
			//    myMenu.Name = this.Request["name"];
			//    myMenu.Description = this.Request["description"];
			//    myMenu.Price = this.Request["price"];
			//    myMenu.Type = this.Request["type"];
			//    myMenu.Size = this.Request["size"];

				//myMenu.Name = name;
				//myMenu.Description = description;
				//myMenu.Price = price;
				//myMenu.Type = type;
				//myMenu.Size = size;

				//InsertMenu(name, description, price, type, size);
			


			//Albums = this.storeDB.Albums.ToList()


			return View(viewModel);


		   
			
		}       

			//public ActionResult Browse(string name, string description, string price, string type, string size)
			//{
			
			//    name = "Glazed Donuts";
			//    description = "Box of six glazed donuts.";
			//    price = "$4.99";
			//    type = "Food";
			//    size = "Medium";
				
				

			//    name = "Double Chocolate Muffin";
			//    description = "A single double chocolate muffin.";
			//    price = "$2.99";
			//    type = "Food";
			//    size = "Medium";

			//    name = "Iced Cookie with Sprinkles";
			//    description = "Delicious iced cookie covered in sprinkles!";
			//    price = "$5.99";
			//    type = "Food";
			//    size = "Medium";

			//    name = "Coffee";
			//    description = "Coffee to help with any morning!";
			//    price = "$3.99";
			//    type = "Beverage";
			//    size = "Small";

			//    name = "Hot Chocolate";
			//    description = "For those cold days enjoy our hot chocolate!";
			//    price = "$2.99";
			//    type = "Beverage";
			//    size = "Small";



			
				
			//    //MenuItemList myMenu = new MenuItemList();

				
			//    //    myMenu.Name = this.Request["name"];
			//    //    myMenu.Description = this.Request["desccription"];
			//    //    myMenu.Price = this.Request["price"];
			//    //    myMenu.Type = this.Request["type"];
			//    //    myMenu.Size = this.Request["size"];

			//    //    InsertMenu(name, description, price, type, size);
				
				
				 
			//    //Albums = this.storeDB.Albums.ToList()
			

			//    return this.View();
			//}

			//private void InsertMenu(string name, string description, string price, string type, string size)
			//{
				
			//}



	}
}
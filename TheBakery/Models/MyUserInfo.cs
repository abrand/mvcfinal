﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;


namespace TheBakery.Models
{
    public class MyUserInfo
    {

        //name
        //email
        //password

        [Required]
        [StringLength(160)]
        public string firstName { get; set; }

        [Required]
        [StringLength(160)]
        public string lastName { get; set; }
        

        public string email { get; set; }

        public int age { get; set; }

        public string password { get; set; }
    }
}